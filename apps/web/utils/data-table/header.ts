import type { VDataTable } from "vuetify/components";

export type TableHeaders = VDataTable["$props"]["headers"];
